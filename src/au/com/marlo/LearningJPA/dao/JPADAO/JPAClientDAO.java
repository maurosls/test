package au.com.marlo.LearningJPA.dao.JPADAO;

import java.util.List;

import au.com.marlo.LearningJPA.dao.ClientDAO;
import au.com.marlo.LearningJPA.model.Client;
import au.com.marlo.LearningJPA.model.Product;
import au.com.marlo.LearningJPA.service.PersistenceManager;

public class JPAClientDAO extends JPADAOGeneric implements ClientDAO{

	public JPAClientDAO (PersistenceManager pm)
	{
		this.pm = pm;
		this.em = this.pm.getEntityManager();
	}
	
	public void update(Client cl, int key) {
		this.tx = this.em.getTransaction();
		this.tx.begin();
		
		Client t = this.em.find(Client.class, key);
		
		t.setAge(cl.getAge());
		t.setAvaiable_account_credit(cl.getAvaiable_account_credit());
		t.setGender(cl.getGender());
		t.setName(cl.getName());	
		
		this.em.persist(t);
		this.tx.commit();
	}
	
	
	public void delete(int key) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		Client ob = em.merge(new Client(key));
		em.remove(ob);
		this.tx.commit();
	}
	
	public int getKeyByName(String name) {
		List key = this.em.createQuery("SELECT c FROM Client c  WHERE c.age = 23").getResultList();
		int k = 0;
		int i =0;
		while(i < key.size()) {
			Client o = (Client) key.iterator().next();
			k = o.getDocument_id();
			i++;
		}
		return k;
	}

	public List<Product> getListOfProducts(String name, JPAOrderDAO jpaOrderDAO) 
	{
		
		this.tx = this.em.getTransaction();
		this.tx.begin();
		List<Product> pds = this.em.createQuery("SELECT p "
				+ " FROM Product p , Order o, Client c "
				+ " left outer join fetch o.products WHERE c.name = :cli").setParameter("cli", name)
				.getResultList();
		this.tx.commit();
		return pds;
	}
}
