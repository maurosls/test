	package au.com.marlo.LearningJPA.dao.JPADAO;

import java.util.List;

import au.com.marlo.LearningJPA.dao.OrderDAO;
import au.com.marlo.LearningJPA.model.Client;
import au.com.marlo.LearningJPA.model.Order;
import au.com.marlo.LearningJPA.model.Product;
import au.com.marlo.LearningJPA.service.PersistenceManager;

public class JPAOrderDAO extends JPADAOGeneric implements OrderDAO{

	public JPAOrderDAO(PersistenceManager pm) 
	{
		this.pm = pm;
		this.em = this.pm.getEntityManager();
	}
	
	public void delete(int key) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		Order ob = em.merge(new Order(key));
		em.remove(ob);
		this.tx.commit();
	}
	
	public void deleteByClient(int client_id) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		this.em.createQuery("DELETE FROM Order o "
				+ "WHERE o.client = :cli").setParameter("cli", client_id)
			.executeUpdate();
		this.tx.commit();
	}
	
	/*
	public List<Product> getListOfProducts(int client_id) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		List<Product> pds = this.em.createQuery("SELECT p FROM Product p, Client c, Order o "
				+ "WHERE c.document_id = o.client_code and p.code = o.product_code and c.document_id = :cli").setParameter("cli", client_id)
				.getResultList();
		this.tx.commit();
		return pds;
	}
	
	public void addListOfProducts(Order or, List<Product> pds, Client client) 
	{
		this.tx = this.em.getTransaction();
		this.tx.begin();
		for(Product p : pds) {
			or = new Order(p.getCode(), client.getDocument_id());
			this.em.persist(or);
		}
		this.tx.commit();
	}
	*/

	public void deleteWithProduct(int code) {
		this.tx.begin();
		this.em.createQuery("DELETE FROM Order o "
				+ " WHERE o.product_code = :code").setParameter("code", code).executeUpdate();
		this.tx.commit();
		
	}
}
